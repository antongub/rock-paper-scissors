import 'package:flutter/cupertino.dart';
import 'dart:math';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:rock_paper_scissors/assetConfig.dart';
import 'package:shake_event/shake_event.dart';
import 'package:flutter_vibrate/flutter_vibrate.dart';

class Hand extends StatefulWidget {
  @override
  _HandState createState() => _HandState();
}

class _HandState extends State<Hand>
    with TickerProviderStateMixin, ShakeHandler {
  String currentImage = basicConfig['rock'];
  int counter = 0;
  bool blocked = false;
  bool _canVibrate = true;
  String _tooltip = "Schnick";

  AnimationController _shakeController;
  AnimationController _finalShakeController;
  Animation _growAnimation;
  Animation _moveAnimation;

  @override
  void initState() {
    super.initState();
    _shakeController = AnimationController(
      duration: const Duration(milliseconds: 500),
      vsync: this,
    )..addListener(() {
        setState(() {});
      });

    _finalShakeController = AnimationController(
      duration: const Duration(milliseconds: 500),
      vsync: this,
    )..addListener(() {
        setState(() {});
      });

    _moveAnimation = TweenSequence([
      TweenSequenceItem(tween: Tween<double>(begin: 0.0, end: 20.0), weight: 1),
      TweenSequenceItem(
          tween: Tween<double>(begin: 20.0, end: -20.0), weight: 1),
      TweenSequenceItem(
          tween: Tween<double>(begin: -20.0, end: 0.0), weight: 1),
    ]).animate(_shakeController);

    _growAnimation = TweenSequence([
      TweenSequenceItem(
          tween: Tween<double>(begin: 100.0, end: 50.0), weight: 1),
      TweenSequenceItem(
          tween: Tween<double>(begin: 50.0, end: 100.0), weight: 1),
    ]).animate(_finalShakeController);

    configureVibration();
  }

  configureVibration() async {
    bool canVibrate = await Vibrate.canVibrate;
    setState(() {
      _canVibrate = canVibrate;
      _canVibrate
          ? print("This device can vibrate")
          : print("This device cannot vibrate");
    });
  }

  @override
  Widget build(BuildContext context) {
    startListeningShake(15);
    return Container(
        child: Transform.translate(
      offset: Offset(_moveAnimation.value, 0.0),
      child: IconButton(
        icon: Image.asset(currentImage),
        iconSize: _growAnimation.value,
        onPressed: handleShake,
        tooltip: _tooltip,
      ),
    ));
  }

  @override
  shakeEventListener() {
    handleShake();
    return super.shakeEventListener();
  }

  @override
  void dispose() {
    resetShakeListeners();
    super.dispose();
    _shakeController.dispose();
  }

  void shake() {
    resetImage();
    var _type = FeedbackType.medium;
    Vibrate.feedback(_type);
    _shakeController.reset();
    _shakeController.forward();
  }

  void finalShake() {
    Vibrate.vibrate();
    randomizeImage();
    _finalShakeController.reset();
    _finalShakeController.forward();
  }

  void resetImage() {
    setState(() {
      this.currentImage = basicConfig['rock'];
    });
  }

  void randomizeImage() {
    Random random = new Random();
    int randomNumber = random.nextInt(basicConfig.length);
    String randomImage = basicConfig.values.elementAt(randomNumber);

    setState(() {
      this.currentImage = randomImage;
    });
  }

  void handleShake() {
    if (blocked == false) {
      counter++;
      if (counter < 3) {
        if (counter == 1) {
          _tooltip = "Schnick";
        }
        if (counter == 2) {
          _tooltip = "Schnack";
        }
        shake();
      } else {
        finalShake();
        _tooltip = "Schnuck";
        counter = 0;
      }
      blocked = true;
      // blocks the shake to trigger for 500 milliseconds
      Timer(Duration(milliseconds: 500), () => blocked = false);
    }
  }
}
