import 'package:flutter/material.dart';
import 'package:rock_paper_scissors/hand.dart';

void main() => runApp(MaterialApp(
  title: "Rock Paper Scissors",
  home: Battleground(),
));

class Battleground extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightBlue[300],
      body: Center(
        child: Hand(),
      ),
    );
  }
}